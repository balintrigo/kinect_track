#include "Kinect.h"
#include <math.h>
#include <iostream>
#include <QtGui/QVector3D>
#include <QtCore/QDebug>

//public
using namespace cv;
using namespace std;

d3Point latestPosition;


inline double calcDistance(Point p1, Point p2)
{
   return sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y));
}

bool cmpDistance(const vector<Point>  &contour1, const vector<Point>  &contour2)
{
   Rect rect1 = boundingRect(Mat(contour1));
   Rect rect2 = boundingRect(Mat(contour2));
   Point center1 = Point(rect1.x + rect1.width / 2, rect1.y + rect1.height /2);
   Point center2 = Point(rect2.x + rect2.width / 2, rect2.y + rect2.height /2);
   double dist1 = calcDistance(center1, Point(latestPosition.x, latestPosition.y));
   double dist2 = calcDistance(center2, Point(latestPosition.x, latestPosition.y));
   return dist1 < dist2;
}

bool cmpArea(const vector<Point>  &contour1, const vector<Point>  &contour2)
{
   return contourArea(Mat(contour1)) > contourArea(Mat(contour2));
}

QKinect::QKinect()
{
   // Register types used in signal/slot mechanism
   qRegisterMetaType<KinectStatus>( "KinectStatus" );

   status = Idle;
}

QKinect::~QKinect()
{
   stop();
}

void QKinect::start()
{
   t_requeststop=false;
   status=Idle;
   QThread::start();
}

void QKinect::stop()
{
   t_requeststop=true;
   wait();
}


d3Point QKinect::getPosition()
{
   QMutexLocker locker(&mutex);
   return latestPosition;
}

double QKinect::getAngle()
{
   QMutexLocker locker(&mutex);
   return angle;
}

QImage QKinect::getRGB()
{
   QMutexLocker locker(&mutex);
   return imgRGB;
}

QString QKinect::getErrorMsg()
{
   QMutexLocker locker(&mutex);
   return errorMsg;
}

bool QKinect::isRunning()
{
   if(QThread::isRunning())
      return true;
   return false;

}

//private
void QKinect::run()
{

   Mat rgbMat(480, 640, CV_8UC3 );	//RGB image
   Mat hsvMat(480, 640, CV_8UC3 );	//HSV image
   Mat yMat( 480, 640, CV_8UC1 );		//yellow-thresholded image
   Mat depth16Mat( 480, 640, CV_16UC1 );	//original 16-bit depth map
   Mat depthMat( 480, 640,CV_8UC1 );	//converted 8-bit depth map


   mutex.lock();
   status=Initializing;
   emit statusNotification(status);
   mutex.unlock();
   bool ok = initialize();

   if(!ok)
   {
      mutex.lock();
      status = ErrorStop;
      emit statusNotification(status);
      mutex.unlock();
      return;
   }

   mutex.lock();
   status = OkRun;
   emit statusNotification(status);
   mutex.unlock();

   while(!t_requeststop)
   {

      bool invalid = false;

      XnStatus status = g_Context.WaitOneUpdateAll(g_ImageGenerator);
      mutex.lock();
      xn::DepthMetaData depthMD;
      xn::ImageMetaData imgMD;
      g_DepthGenerator.GetMetaData(depthMD);
      g_ImageGenerator.GetMetaData(imgMD);

      //Copy OpenNI data to OpenCV Mat variables
      memcpy( rgbMat.data, imgMD.Data(), 640*480*3 );
      memcpy( depth16Mat.data, depthMD.Data(), 640*480*2 );

      //Convert RGB frame to HSV
      cvtColor( rgbMat, rgbMat, CV_RGB2BGR );
      cvtColor( rgbMat, hsvMat, CV_BGR2HSV );



      //Convert depth map to 8-bit Mat
      depth16Mat.convertTo( depthMat, CV_8U, 255/2096.0 );

      //Gaussian blur to remove noise
      blur(hsvMat, hsvMat, Size(13,13));

      //Threshold for yellow and find blobs
      inRange(hsvMat, hsvMin, hsvMax, yMat);


      findContours( yMat, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );

      if(contours.size() >= numMarkers){

		  //Sort blobs from largest to smallest
    	  if(!objectFound || numMarkers == DUAL_MARKER){
    		  sort(contours.begin(), contours.end(), cmpArea);
    	  }else{
    		  sort(contours.begin(), contours.end(), cmpDistance);
    	  }
		  //initialize bounding rectangles of two largest blobs


    	  rect1 = boundingRect(Mat(*(contours.begin())));

    	  center1 = Point(rect1.x + rect1.width / 2, rect1.y + rect1.height /2);

    	  double dist = calcDistance(center1,prevPoint);


    	  if(objectFound && dist > (prevDist+100))
    	  {
    		  qDebug() << "toomuch";
    		  objectFound = false;
    		  invalid = true;
    	  }

    	  prevDist = dist;

		  if(numMarkers == DUAL_MARKER){
			  rect2 = boundingRect(Mat(*(contours.begin() + 1)));
			  if(rect1.x > rect2.x)
			  {
				  Rect tmpRect = rect1;
				  rect1 = rect2;
				  rect2 = tmpRect;
			  }

		  }else{
			  /////////UPDATE HSV constraints
			  if(contours.size() > 1)
			  contours.erase (contours.begin()+1,contours.end());
			  Mat mask = Mat::zeros(hsvMat.rows, hsvMat.cols, CV_8UC1);
			  drawContours(mask, contours, -1, Scalar(255), CV_FILLED);
			  Scalar hsvMean = mean(hsvMat,mask);
			  hsvMin = Scalar(hsvMean[0]- 10, hsvMean[1]- 30,hsvMean[2]- 30);
			  hsvMax = Scalar(hsvMean[0]+ 10, hsvMean[1]+ 30,hsvMean[2]+ 30);
		  }


				//find centres of blobs
		  if(numMarkers == DUAL_MARKER){
			  center2 = Point(rect2.x + rect2.width / 2, rect2.y + rect2.height /2);
			  Point *leftPoint = 0;
			  Point *rightPoint = 0;


			  d3Point vecA;
			  vecA.x = center2.x - center1.x;
			  vecA.y = center2.y - center1.y;
			  vecA.z = depthMD(center2.x,center2.y) - depthMD(center1.x,center1.y);

			  double magA = sqrt(vecA.x*vecA.x + vecA.y*vecA.y + vecA.z*vecA.z);

			  angle = asin((double)vecA.y / magA) * 180.0 / 3.1415926;

		  }


		  if(numMarkers==SINGLE_MARKER){
			  latestPosition.z = depthMD(center1.x,center1.y);
			  latestPosition.x = (center1.x  - cx_rgb) * latestPosition.z / fx_rgb;
			  latestPosition.y = (center1.y - cy_rgb) * latestPosition.z / fy_rgb;
		  }else{
			  latestPosition.z = (depthMD(center1.x,center1.y) + depthMD(center2.x,center2.y)) / 2;
			  latestPosition.x = ((center1.x + center2.x)/2 - cx_rgb) * latestPosition.z / fx_rgb;
			  latestPosition.y = ((center1.y + center2.y)/2 - cy_rgb) * latestPosition.z / fy_rgb;
		  }

		  if(objectFound){
			  prevPoint = center1;
		  }

		  if(!invalid)
		  {
			  objectFound = true;
		  }

      }

      imgRGB = createCameraImage(imgMD);

      if(QDebugOutput == true)
      {
    	  qDebug("%f,%f,%f;",latestPosition.x,latestPosition.y,latestPosition.z);
      }

      emit dataNotification();
      mutex.unlock();
   }
   g_Context.Shutdown();

   mutex.lock();
   status = Idle;
   emit statusNotification(status);
   mutex.unlock();
}

bool QKinect::initialize()
{

   XnStatus nRetVal = XN_STATUS_OK;

   nRetVal = g_Context.Init();
   if(nRetVal!=XN_STATUS_OK)
   {
      mutex.lock();
      errorMsg = QString("Context creation failed: %1").arg(xnGetStatusString(nRetVal));
      qDebug() << errorMsg;
      mutex.unlock();
      return false;
   }

   nRetVal = g_ImageGenerator.Create(g_Context);
   if(nRetVal!=XN_STATUS_OK)
   {
      mutex.lock();
      errorMsg = QString("Image generator creation failed: %1").arg(xnGetStatusString(nRetVal));
      qDebug() << errorMsg;
      mutex.unlock();
      return false;
   }

   nRetVal = g_DepthGenerator.Create(g_Context);
   if(nRetVal!=XN_STATUS_OK)
   {
      mutex.lock();
      errorMsg = QString("Depth generator creation failed: %1").arg(xnGetStatusString(nRetVal));
      qDebug() << errorMsg;
      mutex.unlock();
      return false;
   }


   g_DepthGenerator.GetAlternativeViewPointCap().SetViewPoint(g_ImageGenerator);


   nRetVal = g_Context.StartGeneratingAll();

   objectFound = false;
   numMarkers = SINGLE_MARKER;
   QDebugOutput = false;

   hsvMin = Scalar(12,140,140);
   hsvMax = Scalar(30,225,225);

   prevDist = 1000000;

   return true;
}



QImage QKinect::createCameraImage(const xn::ImageMetaData &imd)
{
   // Here must mutex / run also access the data


   XnUInt16 g_nXRes = 640;
   XnUInt16 g_nYRes = 480;

   QImage image(g_nXRes,g_nYRes,QImage::Format_RGB32);


   const XnUInt8 *idata = imd.Data();
   for (unsigned nY=0; nY<g_nYRes; nY++)
   {
      uchar *imageptr = image.scanLine(nY);

      for (unsigned nX=0; nX < g_nXRes; nX++)
      {
         imageptr[0] = idata[2];
         imageptr[1] = idata[1];
         imageptr[2] = idata[0];
         imageptr[3] = 0xff;

         imageptr+=4;
         idata+=3;

      }
   }

   if(objectFound){
	   QPainter p;
	   QPen rectPen = QPen(Qt::red);
	   rectPen.setWidth(4);
	   p.begin(&image);
	   p.setPen(rectPen);
	   p.drawRect(QRect(rect1.x,rect1.y,rect1.width,rect1.height));
	   if(numMarkers == DUAL_MARKER)
		   p.drawRect(QRect(rect2.x,rect2.y,rect2.width,rect2.height));
	   p.end();
   }
   return image;

}


