#include "MainWindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :  QMainWindow(parent),  ui(new Ui::MainWindow)
{
   ui->setupUi(this);

   // Setup the kinect reader
   connect(&kinect,SIGNAL(dataNotification()),this,SLOT(kinectData()));
   connect(&kinect,SIGNAL(statusNotification(KinectStatus)),this,SLOT(kinectStatus(KinectStatus)));

   sbKinectStatus = new QLabel(statusBar());
   ui->statusbar->addWidget(sbKinectStatus);
}

MainWindow::~MainWindow()
{
   delete ui;
}

void MainWindow::kinectData()
{

   QImage img = kinect.getRGB();
   d3Point pos = kinect.getPosition();
   double angle = kinect.getAngle();
   ui->labelImage->setPixmap(QPixmap::fromImage(img));
   ui->outputBox->appendPlainText(QString::number(pos.x,'.',4) + "\t" + QString::number(pos.y,'.',4) + "\t" + QString::number(pos.z,'.',4) + "\t" + QString::number(angle,'.',4)+ "  \n");

}
void MainWindow::kinectStatus(KinectStatus s)
{
   QString str("Kinect: ");
   if(s==Idle)
      str += "Idle";
   if(s==Initializing)
      str += "Initializing";
   if(s==OkRun)
      str += "Running";
   if(s==ErrorStop)
      str += "Error";

   if(s==ErrorStop || s==OkRun || s==Idle)
         ui->pushButton_StartStop->setEnabled(true);
   if(s==OkRun)
         ui->pushButton_StartStop->setText("Stop");
   if(s==ErrorStop || s==Idle)
         ui->pushButton_StartStop->setText("Start");

   sbKinectStatus->setText(str);

}

void MainWindow::on_pushButton_StartStop_clicked()
{
   ui->pushButton_StartStop->setEnabled(false);

   if(kinect.isRunning())
   {
      kinect.stop();
   }
   else
   {
      kinect.start();
   }

}

void MainWindow::on_radio_Marker1_toggled()
{
	   if(ui->radio_Marker1->isChecked()==true)
		   kinect.numMarkers = SINGLE_MARKER;
	   else
		   kinect.numMarkers = DUAL_MARKER;
}

void MainWindow::on_radio_Marker2_toggled()
{
	 on_radio_Marker1_toggled();
}

void MainWindow::on_check_OutputQDebug_stateChanged()
{
   if(ui->check_OutputQDebug->isChecked()==true)
	   kinect.QDebugOutput = true;
   else
	   kinect.QDebugOutput = false;
}


