#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <QtGui/QMainWindow>
#include <QtGui/QLabel>
#include "Kinect.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();

public slots:
   void kinectData();
   void kinectStatus(KinectStatus);


private slots:
   void on_pushButton_StartStop_clicked();
   void on_radio_Marker1_toggled();
   void on_radio_Marker2_toggled();
   void on_check_OutputQDebug_stateChanged();

private:
   Ui::MainWindow *ui;
   QKinect kinect;
   QLabel *sbKinectStatus;

protected:
};


#endif /* MAINWINDOW_H_ */
