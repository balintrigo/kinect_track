/*
 * Kinect.h
 *
 *
 *      Author: Balint Rigo
 */

#ifndef KINECT_H_
#define KINECT_H_



#include <QtGui/QWidget>
#include <QtGui/QPainter>
#include <QtCore/QThread>
#include <QtCore/QMutex>
#include <XnOpenNI.h>
#include <XnCppWrapper.h>
#include <XnCppWrapper.h>
#include <opencv2/opencv.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <vector>


enum KinectStatus
   {
      Idle = 0,
      Initializing = 1,
      OkRun = 2,
      ErrorStop = 3
   };

enum MarkersNumber
   {
      SINGLE_MARKER = 1,
      DUAL_MARKER = 2
   };

struct d3Point
{

  double x, y, z;

};

extern d3Point latestPosition;

const double fx_rgb = 529.21508098293293;
const double fy_rgb = 525.56393630057437;
const double cx_rgb = 328.94272028759258;
const double cy_rgb = 267.48068171871557;


class QKinect : public QThread
{
	Q_OBJECT
public:
	QKinect();
	~QKinect();

	void run();
    void start();
    void stop();

    bool isRunning();

    QImage  getRGB();
    d3Point getPosition();
    double  getAngle();
    QString getErrorMsg();

    bool QDebugOutput;
    MarkersNumber numMarkers;


private:


    cv::Scalar hsvMin;
    cv::Scalar hsvMax;

    QImage imgDepth, imgRGB;
    QString errorMsg;

    bool t_requeststop;
    QMutex mutex;
    volatile KinectStatus status;    // 0: pre-init. 1: init. 2: ok-run. 3:error-stop
    xn::Context g_Context;
    xn::DepthGenerator g_DepthGenerator;
    xn::ImageGenerator g_ImageGenerator;

    QImage createDepthImage();
    QImage createCameraImage(const xn::ImageMetaData &imd);


    std::vector<std::vector<cv::Point> > contours;
    cv::Rect rect1;
    cv::Rect rect2;
    cv::Point center1;
    cv::Point center2;

    double angle;

    bool objectFound;

    double prevDist;
    cv::Point prevPoint;

    bool initialize();

private slots:

signals:
     void dataNotification();
     void statusNotification(KinectStatus);
};

Q_DECLARE_METATYPE(KinectStatus);

#endif /* KINECT_H_ */
